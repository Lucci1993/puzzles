﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseRotation : MonoBehaviour {

    public RectTransform center;
    public float maxVelocity;

    PathFollower follower;
    float angle;
    float speed;
    float offset;
    bool switching;

	// Use this for initialization
	void Start () {
        Cursor.visible = false;
        follower = PathFollower.instance;
        offset = Vector2.Distance(transform.position, center.position);
        speed = (2f * Mathf.PI) / maxVelocity;
	}
	
	// Update is called once per frame
	void Update () {
        if (!follower.stopGame) {
            // if true
            follower.isPressed |= Input.GetMouseButtonDown(0);
            // if false
            follower.isPressed &= !Input.GetMouseButtonUp(0);
            if (Input.GetKeyDown(KeyCode.Escape)) {
                Cursor.visible = !Cursor.visible;
            }
            if (Input.GetKeyDown(KeyCode.LeftShift)) {
                switching = !switching;
            }
            CalculateCirclePosition();
            transform.rotation = follower.CalculateLookAtZ(transform.position, center.position, 90f);
            follower.rotation = transform.eulerAngles.z;

        }
	}

    void CalculateCirclePosition() {
        Vector2 mouseVelocity = MouseForce();
        if (switching) {
            angle += (mouseVelocity.x + mouseVelocity.y) * Time.deltaTime;
        }
        else {
            angle -= (mouseVelocity.x + mouseVelocity.y) * Time.deltaTime;
        }
        float x = (Mathf.Cos(angle) * offset) + center.position.x;
        float y = (Mathf.Sin(angle) * offset) + center.position.y;
        transform.position = new Vector3(x, y, 0f);
    }

    Vector2 MouseForce() {
        Vector3 mouse = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        Vector3 centerPoint = Camera.main.ScreenToViewportPoint(center.position);
        float x = Mathf.Clamp(Mathf.Abs(mouse.x - centerPoint.x), 0f, speed);
        float y = Mathf.Clamp(Mathf.Abs(mouse.y - centerPoint.y), 0f, speed);
        return new Vector2(x, y);
    }
}
