﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using System;

public class PathFollower : MonoBehaviour {

    public GameObject knob;
    public Text tutorial;
    public Text level;
    public Path[] paths;

    [HideInInspector]
    public bool isPressed;
    [HideInInspector]
    public bool stopGame;
    [HideInInspector]
    public float rotation;
    [HideInInspector]
    public Path currentPath;

    int stepCounter;
    RectTransform currentPoint;
    RectTransform nextPoint;
    float speed;
    float timer;
    bool knobIsActive;

    const float DISTR = 1f / 180f;

    public static PathFollower instance;

    void Awake() {
        // set the singleton
        if (instance != null) {
            return;
        }
        instance = this;
    }

	// Use this for initialization
	void Start () {
        StartNextPath(stepCounter);
	}
	
	// Update is called once per frame
	void Update () {
        if (!stopGame) {
            // change level number
            level.text = "Level " + (stepCounter+1);
            if (!knobIsActive) {
                if (isPressed) {
                    // restart all
                    stepCounter = 0;
                    StartNextPath(stepCounter);
                    currentPath.RestartPath();
                }
                // fusible is in the middle at the core
                if (currentPath.isLastPoint && currentPath.isFirstPath) {
                    currentPath.fusible.gameObject.SetActive(false);
                    StartCoroutine("ShowKnob");
                    currentPath.isFirstPath = false;
                }
                // fusible is at the end
                else if (currentPath.isLastPoint && !currentPath.isFirstPath) {
                    currentPath.fusible.gameObject.SetActive(false);
                    currentPath.RestartPath();
                    currentPoint = currentPath.StartPoint();
                    nextPoint = null;
                }
                else {
                    if (nextPoint != null && Vector2.Distance(nextPoint.position, currentPath.fusible.transform.position) > 0.01f) {
                        timer += Time.deltaTime / (speed * Circle());
                        currentPath.fusible.transform.position = Vector2.Lerp(currentPoint.position, nextPoint.position, timer);
                    }
                    else {
                        CalculateTrajectory();
                    }
                }
            }
            else if (isPressed && knobIsActive) {
                // if knob is active move to the next step
                stepCounter++;
                if (stepCounter < paths.Length) {
                    StopCoroutine("ShowKnob");
                    knob.SetActive(false);
                    knobIsActive = false;
                    isPressed = false;
                    StartNextPath(stepCounter);
                }
                else {
                    tutorial.text = "You Won!";
                    stopGame = true;
                    Cursor.visible = true;
                }
            }
        }
	}

    // start the new path
    void StartNextPath(int counter) {
        currentPath = paths[counter];
        currentPath.CreatePath();
        currentPoint = currentPath.StartPoint();
        nextPoint = null;
        currentPath.RestartPath();
    }

    // use a distribution normalized function
    float Circle() {
        float a;
        if (currentPath.degrees <= 180f) {
            if (rotation >= 0f && rotation < currentPath.degrees) {
                a = currentPath.degrees - 180f;
                return DISTR * (rotation - a);
            }
            if (rotation >= currentPath.degrees && rotation <= currentPath.degrees + 180f) {
                a = currentPath.degrees;
                return 1f - (DISTR * (rotation - a));
            }
            a = currentPath.degrees + 180f;
            return DISTR * (rotation - a);
        }
        // else for degrees > 180
        if (rotation >= 0f && rotation < (currentPath.degrees - 180f)) {
            a = currentPath.degrees - 360f;
            return 1f - (DISTR * (rotation - a));
        }
        if (rotation >= (currentPath.degrees - 180f) && rotation <= currentPath.degrees) {
            a = currentPath.degrees - 180f;
            return DISTR * (rotation - a);
        }
        a = currentPath.degrees;
        return 1f - (DISTR * (rotation - a));
    }

    // calculate the fuse position
    void CalculateTrajectory() {
        // get next point
        if (nextPoint != null) {
            currentPoint = nextPoint;
        }
        nextPoint = currentPath.NextPoint();
        // look at
        currentPath.fusible.rotation = CalculateLookAtZ(currentPath.fusible.position, nextPoint.position, 0f);
        float distance = Vector2.Distance(nextPoint.position, currentPoint.position);
        speed = currentPath.time * (distance/currentPath.maxDistance);
        timer = 0;
    }

    // show the knob for some seconds
    IEnumerator ShowKnob() {
        knob.SetActive(true);
        knobIsActive = true;
        yield return new WaitForSeconds(currentPath.time * Circle());
        // start path
        if (!stopGame) {
            knob.SetActive(false);
            // start with next fusible
            currentPath.fusible.position = currentPath.MiddlePoint().position;
            currentPath.fusible.gameObject.SetActive(true);
            CalculateTrajectory();
            knobIsActive = false;
        }
    }

    public Quaternion CalculateLookAtZ(Vector3 position1, Vector3 position2, float offset) {
        // look at
        Vector3 difference = position1 - position2;
        float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        return Quaternion.Euler(0.0f, 0.0f, rotationZ + offset);
    }
}

[Serializable]
public class Path {
    public RectTransform fusible;
    [Range(0.1f, 2f)] public float time;
    [Range(0f, 359f)] public float degrees;
    public RectTransform firstPath;
    public RectTransform secondPath;

    [HideInInspector]
    public float maxDistance;
    [HideInInspector]
    public bool isLastPoint;
    [HideInInspector]
    public bool isFirstPath;

    RectTransform[] firstPathArray;
    RectTransform[] secondPathArray;
    int count;

    public void CreatePath() {
        firstPathArray = firstPath.GetComponentsInChildren<RectTransform>().Skip(1).ToArray();
        secondPathArray = secondPath.GetComponentsInChildren<RectTransform>().Skip(1).ToArray();
        maxDistance = CalculateMaxDistance();
        isFirstPath = true;
    }

    // calculate the max distance for next calculation with time
    float CalculateMaxDistance() {
        float[] distances = new float[firstPathArray.Length-1 + secondPathArray.Length-1];
        int counter = 0;
        bool isFirst = true;
        for (int i = 0; i < distances.Length; i++) {
            // if the first path
            if (isFirst) {
                // if it's not the end of the first array
                if (counter != firstPathArray.Length - 1) {
                    distances[i] = Vector2.Distance(firstPathArray[counter].position, firstPathArray[counter+1].position);
                    counter++;
                }
                else {
                    // if it's the last move to the next array
                    isFirst = false;
                    counter = 0;
                    i--;
                }
            }
            else {
                // if it's not the end of the second array
                if (counter != firstPathArray.Length-1) {
                    distances[i] = Vector2.Distance(secondPathArray[counter].position, secondPathArray[counter+1].position);
                    counter++;
                }
            }
        }
        return distances.Max();
    }

    // start of the path
    public RectTransform StartPoint() {
        return firstPathArray[0];
    }

    // middle of the path
    public RectTransform MiddlePoint() {
        return secondPathArray[0];
    }

    // restart the path
    public void RestartPath() {
        fusible.position = firstPathArray[0].position;
        fusible.gameObject.SetActive(true);
        isFirstPath = true;
        isLastPoint = false;
    }

    // move to the next position
    public RectTransform NextPoint() {
        count++;
        // if the first path
        if (isFirstPath) {
            // if it's not the end of the first array
            if (count != firstPathArray.Length) {
                isLastPoint = false;
                return firstPathArray[count];
            }
            // if it's the last move to the next array
            count = 0;
            isLastPoint = true;
            return secondPathArray.First();
        }
        // if it's not the end of the second array
        if (count != secondPathArray.Length) {
            isLastPoint = false;
            return secondPathArray[count];
        }
        // start from beginning
        count = 0;
        isLastPoint = true;
        return firstPathArray.First();
    }
}
