﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonScripts : MonoBehaviour {

    // came back to main menu
    public void ExitPuzzle() {
        SceneManager.LoadScene(0);
    }

    // came back to main menu
    public void EnterPuzzleOne() {
        SceneManager.LoadScene(1);
    }

    // came back to main menu
    public void EnterPuzzleTwo() {
        SceneManager.LoadScene(2);
    }

    // exit game
    public void ExitGame() {
        Application.Quit();
    }
}
