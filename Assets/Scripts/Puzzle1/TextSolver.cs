﻿using UnityEngine;
using UnityEngine.UI;

public class TextSolver : MonoBehaviour {

    public string message;
    public Text tutorial;
    [Range(0, 26)] public int startShifting;
    [HideInInspector]
    public bool gameStop;
    public static TextSolver instance;

    Text text;

    void Awake() {
        // set the singleton
        if (instance != null) {
            return;
        }
        instance = this;
    }

    // Use this for initialization
    void Start () {
        text = transform.GetComponent<Text>();
        message.ToLower();
        Caesar(1);
	}
	
    // create a Caesar cipher
    public void Caesar(int shift) {
        char[] buffer = message.ToCharArray();
        for (int i = 0; i < buffer.Length; i++) {
            // Letter.
            char letter = buffer[i];
            //If it isn't a space
            if(letter != ' '){
                //Add shift to all 
                letter = (char)(letter + shift - startShifting);
                // Subtract 26 on overflow.
                if (letter > 'z') {
                    letter = (char)(letter - 26);
                }
                // Add 26 on underflow.
                else if (letter < 'a') {
                    letter = (char)(letter + 26);
                }
                // Store.
                buffer[i] = letter;
            }
        }
        text.text = new string(buffer).ToUpper();
        // stop game if message is the same
        if (text.text.ToLower() == message) {
            gameStop = true;
            tutorial.text = "You decrypted the message!";
        }
    }
}
