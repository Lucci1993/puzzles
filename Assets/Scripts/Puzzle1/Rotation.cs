﻿using UnityEngine;
using UnityEngine.UI;

public class Rotation : MonoBehaviour {

    public Text number;
    [Range(0.5f, 2)] public float degreesForSeconds;

    TextSolver solver;
    int shifter;

	// Use this for initialization
	void Start () {
        solver = TextSolver.instance;
	}
	
	// Update is called once per frame
	void Update () {
        if (!solver.gameStop) {
            RotateKnob();
        }
	}

    void RotateKnob() {
        float horizontal = Input.GetAxisRaw("Horizontal");
        // set the shifter number
        shifter = (int)((transform.localRotation.eulerAngles.z * 26f) / 360f) + 1;
        number.text = shifter.ToString();
        // rotate if move
        if (horizontal > 0 || horizontal < 0) {
            float rotAmount = degreesForSeconds * horizontal;
            float curRot = transform.localRotation.eulerAngles.z;
            transform.localRotation = Quaternion.Euler(new Vector3(0, 0, curRot + rotAmount));
        }
        else {
            // at stop calculate the shifting number
            solver.Caesar(shifter);
        }
    }
}
